#ifndef CHECK_PROTO_H
# define CHECK_PROTO_H

#include "ft_printf.h"

int n_z(long int nb);
int any_flag(t_printf *var, char *c);
int any_width(t_printf *var, char *c);
int any_length(t_printf *var, char *c);
int any_precision(t_printf *var, char *c);
int any_type(t_printf *var, char *c);
int info_var(t_printf *var, char *c);

#endif