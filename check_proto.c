
#include "check_proto.h"

int    n_z(long int nb)
{
	int	size;
    int i;

    i = 0;
	size = 0;
    if (nb < 0)
    {
        nb = nb * -1;
        i = 1;
    }
	while (nb >= 10)
	{
		nb /= 10;
		++size;
	}
    if (i == 1)
	    return (size + 2);
    return (size + 1);
}

int any_flag(t_printf *var, char *c)
{
    if (*c == '#')
        var->sh = 1;
    if (*c == '0')
        var->z = 1;
    if (*c == '+')
        var->pl = 1;
    if (*c == '-')
        var->m = 1;
    if (*c == ' ')
        var->sp = 1;
    return ((*c == '#' || *c == '0' || *c == '-' || *c == '+' || *c == ' ') ? 1 : 0);
}

int any_width(t_printf *var, char *c)
{
    if (*c <= '9' && *c >= '1')
    {
        var->w = ft_atoi(c);
        return (n_z(var->w));
    }
    return (0);
}

int any_length(t_printf *var, char *c)
{
    if (*c == 'l')
    {
        if (*(c + 1) == 'l')
        {
            var->ll = 1;
            return (2);
        }
        var->l = 1;
    }
    if (*c == 'h')
    {
        if (*(c + 1) == 'h')
        {
            var->hh = 1;
            return (2);
        }
        var->h = 1;
    }
    if (*c == 'L')
        var->L = 1;
    return ((*c == 'l' || *c == 'h' || *c == 'L') ? 1 : 0);
}

int any_precision(t_printf *var, char *c)
{
    if (*c == '.')
    {
        var->pr = ft_atoi(c+1);
        return (n_z(var->pr) + 1); // un 1 ajouté de '.'
    }
    return (0);
}

int any_type(t_printf *var, char *c)
{
    if (TYPE(*c))
    {
        var->type = *c;
        return (1);
    }
    return (0);
}

int    info_var(t_printf *var, char *c)
{
    int a;

    if ((a = any_flag(var, c)) == 0)
    {
        if ((a = any_width(var, c)) == 0)
        {
            if((a = any_precision(var, c)) == 0)
            {
                if ((a = any_length(var, c)) == 0)
                    return (any_type(var, c));
                return (a);
            }
            return (a);
        }
        return (a);
    } 
    return (a);
}