#ifndef FT_PRINTF_H
#define FT_PRINTF_H

#include <stdarg.h>
#include "libft/libft.h"

//*******************
#include <stdio.h>  /*temp library
********************/

int    ft_printf(const char *format, ...);

#define TYPE(t) (t == 'c' || t == 's' || t == 'p' || t == 'd' || t == 'i' \
 || t == 'o' || t == 'u' || t == 'x' || t == 'X' || t == 'f') ? 1 : 0

typedef enum { false, true } bool;

typedef struct   s_printf
{
    char       type;
    int        sh;
    int        z;
    int        pl;
    int        m;
    int        l;
    int        ll;
    int        h;
    int        hh;
    int        L;
    int        sp;
    int        pr;
    int        w;

}               t_printf;


#endif