#include "ft_printf.h"
#include "manup_proto.h"
#include "check_proto.h"

void    print_t_printf(t_printf *va_arg)
{
    printf("\ntype : %c\n# : %d\n0 : %d\n+ : %d\n- : %d\nl : %d\nll : %d\nh : %d\nhh : %d\nL : %d\nspace : %d\nprscision : %d\nw : %d",
    va_arg->type, va_arg->sh, va_arg->z, va_arg->pl, va_arg->m, va_arg->l, va_arg->ll, va_arg->h, va_arg->hh, va_arg->L, va_arg->sp, va_arg->pr, va_arg->w);
}

void    init_printf(t_printf *var_proto)
{
       var_proto->type = 'a';
       var_proto->sh = 0;
       var_proto->z = 0;
       var_proto->pl = 0;
       var_proto->m = 0;
       var_proto->l = 0;
       var_proto->ll = 0;
       var_proto->h = 0;
       var_proto->hh = 0;
       var_proto->L = 0;
       var_proto->sp = 0;
       var_proto->pr = 0;
       var_proto->w = 0;
}


void manage_string(t_printf *var_proto, char *value)
{
    if(var_proto->type == 's')
        ft_putstr(value);
}

int    ft_printf(const char *format, ...)
{
    t_printf *var_proto;
    var_proto = (t_printf*)malloc(sizeof(t_printf));
    int size;
    va_list ap;
    
    init_printf(var_proto);
    va_start(ap, format);
    while (*format)
    {
        if (*format == '%')
        {
            //ft_putchar('%');
            format++;
            while (!TYPE(*format))
            {
                if (*format == '*') // ce cat peut etre uniquement gerè dans ft_printf() 
                {
                    var_proto->w = va_arg(ap, int); 
                    size = 1;
                }
                else
                    size = info_var(var_proto, (char *)format);
                format+= size;
            }
            info_var(var_proto, (char *)format); // get type specifier
                (var_proto->type == 'i' || var_proto->type == 'd' || var_proto->type == 'c' || var_proto->type == 'p' || var_proto->type == 'x') ?
                manup_intiger(var_proto, va_arg(ap,int)) // recoit la valeur d'argument et l' afficher apres y avoir apporté des modifications (pour cas d'entier)  
                : manup_string(var_proto, va_arg(ap,char*));                           
        }
        else
            ft_putchar(*format);
        format++;
    }
    va_end(ap);
    // print_t_printf(var_proto);
    return (0);
}
