/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memset.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yzemmour <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/29 17:55:52 by yzemmour          #+#    #+#             */
/*   Updated: 2019/04/21 23:26:39 by yzemmour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memset(void *pointer, int value, size_t count)
{
	char	*p;
	char	v;

	v = (char)value;
	p = (char *)pointer;
	while (count)
	{
		*p = v;
		p++;
		count--;
	}
	return (pointer);
}
