/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstdeloccur.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yzemmour <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/12 22:13:18 by yzemmour          #+#    #+#             */
/*   Updated: 2019/04/15 19:59:38 by yzemmour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list		*ft_lstdeloccur(t_list *lst, char *data)
{
	t_list *tmp;

	if (lst == NULL)
		return (NULL);
	if (ft_strcmp(lst->content, data))
	{
		tmp = lst->next;
		free(lst);
		tmp = ft_lstdeloccur(tmp, data);
		return (tmp);
	}
	else
	{
		lst->next = ft_lstdeloccur(lst->next, data);
		return (lst);
	}
}
