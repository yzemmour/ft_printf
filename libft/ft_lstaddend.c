/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstaddend.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yzemmour <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/12 06:23:37 by yzemmour          #+#    #+#             */
/*   Updated: 2019/04/26 21:11:34 by yzemmour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstaddend(t_list *lst, t_list *elem)
{
	t_list	*current;

	current = lst;
	elem->next = NULL;
	if (lst == NULL)
		lst = elem;
	else
	{
		while (current->next != NULL)
			current = current->next;
		current->next = elem;
	}
}
