/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yzemmour <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/01 23:51:38 by yzemmour          #+#    #+#             */
/*   Updated: 2019/04/15 16:05:19 by yzemmour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_putnbr(long int n)
{
	long int number;
	char c;

	number = n;
	if (number < 0)
	{
		number = number * (-1);
		write(1, "-", 1);
	}
	if (number > 9)
		ft_putnbr(number / 10);
	c = (number % 10) + '0';
	write(1, &c, 1);
}
