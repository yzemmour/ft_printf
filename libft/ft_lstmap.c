/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yzemmour <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/07 01:58:55 by yzemmour          #+#    #+#             */
/*   Updated: 2019/04/15 16:31:59 by yzemmour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem))
{
	t_list	*ret;
	t_list	*temp;

	if (!lst)
		return (NULL);
	ret = f(lst);
	if (ret == NULL)
		return (NULL);
	temp = ret;
	while (lst->next)
	{
		lst = lst->next;
		temp->next = f(lst);
		if (temp->next == NULL)
		{
			ft_lstdel(&ret, ft_del);
			return (NULL);
		}
		temp = temp->next;
	}
	return (ret);
}
