/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yzemmour <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/30 18:24:39 by yzemmour          #+#    #+#             */
/*   Updated: 2019/04/15 16:47:36 by yzemmour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strchr(const char *s, int c)
{
	char val;

	val = (char)c;
	while (*s != '\0')
	{
		if (*s == val)
			return ((char *)s);
		else
			s++;
	}
	if (*s == val)
		return ((char *)s);
	return (NULL);
}
