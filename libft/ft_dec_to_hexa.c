#include "libft.h"

void ft_dec_to_hexa(int n) 
{    
    char hexaDeciNum[100]; 
    int i;
    int j;
    int temp;

    i = 0;
    temp = 0;
    while(n!=0) 
    {    
        temp = n % 16;   
        if(temp < 10) 
        { 
            hexaDeciNum[i] = temp + 48; 
            i++; 
        } 
        else
        { 
            hexaDeciNum[i] = temp + 87; 
            i++; 
        }  
        n = n/16; 
    }
    j = i - 1;
    while (j >= 0)
    {
        ft_putchar(hexaDeciNum[j]);
        j--;
    }
} 