/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstdelend.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yzemmour <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/12 15:45:28 by yzemmour          #+#    #+#             */
/*   Updated: 2019/04/14 11:18:01 by yzemmour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list		*ft_lstdelend(t_list *lst)
{
	t_list *current;
	t_list *newendelem;

	if (lst == NULL)
		return (NULL);
	if (lst->next == NULL)
	{
		free(lst);
		return (NULL);
	}
	current = lst;
	newendelem = lst;
	while (current->next != NULL)
	{
		newendelem = current;
		current = current->next;
	}
	newendelem->next = NULL;
	free(current);
	return (lst);
}
