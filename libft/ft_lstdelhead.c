/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstdelhead.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yzemmour <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/12 14:49:54 by yzemmour          #+#    #+#             */
/*   Updated: 2019/04/14 11:21:25 by yzemmour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list		*ft_lstdelhead(t_list *lst)
{
	t_list *newlst;

	if (lst == NULL)
		return (NULL);
	else
	{
		newlst = lst->next;
		free(lst);
		free(lst->content);
		return (newlst);
	}
}
