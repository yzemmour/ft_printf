/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr_fd.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yzemmour <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/02 17:52:16 by yzemmour          #+#    #+#             */
/*   Updated: 2019/04/15 16:06:00 by yzemmour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_putnbr_fd(int n, int fd)
{
	long number;
	char c;

	number = n;
	if (number < 0)
	{
		number = number * (-1);
		write(fd, "-", 1);
	}
	if (number > 9)
		ft_putnbr_fd(number / 10, fd);
	c = (number % 10) + '0';
	write(fd, &c, 1);
}
