/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yzemmour <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/31 18:34:12 by yzemmour          #+#    #+#             */
/*   Updated: 2019/04/15 16:33:44 by yzemmour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strmap(char const *s, char (*f)(char))
{
	char	*p;
	int		i;

	i = 0;
	if (s)
	{
		p = (char *)malloc(ft_strlen(s) + 1);
		if (p != NULL)
		{
			while (s[i])
			{
				p[i] = f(s[i]);
				i++;
			}
			p[i] = '\0';
			return (p);
		}
	}
	return (NULL);
}
