/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yzemmour <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/30 02:30:30 by yzemmour          #+#    #+#             */
/*   Updated: 2019/04/29 17:59:24 by yzemmour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strdup(const char *src)
{
	char	*p;
	int		i;

	i = 0;
	while (src[i])
		i++;
	p = (char*)malloc((i + 1) * sizeof(char));
	if (p != 0)
	{
		i = 0;
		while (src[i])
		{
			p[i] = (char)src[i];
			i++;
		}
		p[i] = '\0';
	}
	return (p);
}
