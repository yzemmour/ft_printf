/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yzemmour <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/30 00:36:09 by yzemmour          #+#    #+#             */
/*   Updated: 2019/04/15 01:00:32 by yzemmour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memchr(const void *s, int c, size_t n)
{
	const unsigned char	*p;
	unsigned char		val;

	p = (const unsigned char *)s;
	val = (unsigned char)c;
	while (n)
	{
		if (*p == val)
			return ((void *)p);
		else
		{
			p++;
			n--;
		}
	}
	return (NULL);
}
