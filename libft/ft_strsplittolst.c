/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplittolst.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yzemmour <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/10 02:05:09 by yzemmour          #+#    #+#             */
/*   Updated: 2019/04/14 11:31:17 by yzemmour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int		ft_cnt_parts(const char *str, char c)
{
	int		word;
	int		i;

	i = 0;
	word = 0;
	if (!str)
		return (0);
	if (str[i] == c)
		i++;
	while (str[i])
	{
		if (str[i] == c && str[i - 1] != c)
			word++;
		i++;
	}
	if (str[i - 1] != c)
		word++;
	return (word);
}

static int		ft_wlen(const char *s, char c)
{
	int		len;

	len = 0;
	while (*s != c && *s != '\0')
	{
		len++;
		s++;
	}
	return (len);
}

static t_list	*newelem(const char *s, char c)
{
	t_list	*elem;

	elem = malloc(sizeof(t_list));
	if (elem == NULL)
		return (NULL);
	else
	{
		elem->next = NULL;
		elem->content = ft_strsub(s, 0, ft_wlen(s, c));
	}
	return (elem);
}

t_list			*ft_strsplittolst(const char *s, char c)
{
	t_list	*lst;
	t_list	*current;
	int		nb_word;

	lst = NULL;
	nb_word = ft_cnt_parts((const char *)s, c);
	while (nb_word--)
	{
		while (*s == c && *s != '\0')
			s++;
		if (newelem(s, c) == NULL)
			return (NULL);
		if (lst == NULL)
			lst = newelem(s, c);
		else
		{
			current = lst;
			while (current->next != NULL)
				current = current->next;
			current->next = newelem(s, c);
		}
		s = s + ft_wlen(s, c);
	}
	return (lst);
}
