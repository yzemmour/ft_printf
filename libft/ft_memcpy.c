/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yzemmour <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/29 18:46:07 by yzemmour          #+#    #+#             */
/*   Updated: 2019/04/24 12:35:45 by yzemmour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memcpy(void *des, const void *src, size_t size)
{
	char *d;
	char *s;

	d = (char *)des;
	s = (char *)src;
	while (size)
	{
		*d = *s;
		d++;
		s++;
		size--;
	}
	return (des);
}
