/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstfindelem.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yzemmour <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/12 15:56:23 by yzemmour          #+#    #+#             */
/*   Updated: 2019/04/14 11:24:20 by yzemmour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstfindelem(t_list *lst, char *elem)
{
	t_list *current;

	current = lst;
	while (current != NULL)
	{
		if (ft_strcmp(current->content, elem) == 0)
			return (current);
		current = current->next;
	}
	return (NULL);
}
