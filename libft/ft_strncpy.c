/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yzemmour <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/30 03:17:36 by yzemmour          #+#    #+#             */
/*   Updated: 2019/04/15 02:00:18 by yzemmour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strncpy(char *dst, const char *src, size_t len)
{
	char		*d;
	const char	*s;

	s = src;
	d = dst;
	while ((len > 0) && (*s != '\0'))
	{
		*d = *s;
		d++;
		s++;
		len--;
	}
	while (len > 0)
	{
		*d = '\0';
		d++;
		len--;
	}
	return (dst);
}
