/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yzemmour <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/31 21:02:28 by yzemmour          #+#    #+#             */
/*   Updated: 2019/04/15 04:41:06 by yzemmour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strjoin(char const *s1, char const *s2)
{
	int		i;
	int		j;
	char	*p;

	j = 0;
	i = -1;
	if (s1 && s2)
	{
		p = (char *)malloc(ft_strlen(s1) + ft_strlen(s2) + 1);
		if (p != NULL)
		{
			while (s1[++i])
				p[i] = s1[i];
			while (s2[j])
			{
				p[i] = s2[j];
				i++;
				j++;
			}
			p[i] = '\0';
			return (p);
		}
	}
	return (NULL);
}
