/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yzemmour <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/30 18:39:24 by yzemmour          #+#    #+#             */
/*   Updated: 2019/04/15 19:20:07 by yzemmour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strrchr(const char *s, int c)
{
	const char	*p;
	char		val;

	p = NULL;
	val = (char)c;
	if (c == '\0')
		return (ft_strchr(s, '\0'));
	while (*s)
	{
		if (*s == val)
			p = s;
		s++;
	}
	return ((char *)p);
}
