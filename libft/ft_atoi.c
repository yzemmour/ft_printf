/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yzemmour <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/30 22:17:47 by yzemmour          #+#    #+#             */
/*   Updated: 2019/04/15 23:48:03 by yzemmour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_atoi(const char *str)
{
	int		sign;
	long	ret;

	ret = 0;
	sign = 1;
	while (*str == ' ' || *str == '\n' || *str == '\t' || *str == '\v' ||
			*str == '\f' || *str == '\r')
		str++;
	if ((*str == '+') || (*str == '-'))
	{
		if (*str == '-')
			sign = -1;
		str++;
	}
	while ((*str <= '9') && (*str >= '0'))
	{
		ret = ret * 10 + (*str - '0');
		str++;
		if (ret < 0 && sign == 1)
			return (-1);
		if (ret < 0 && sign == -1)
			return (0);
	}
	return (ret * sign);
}
