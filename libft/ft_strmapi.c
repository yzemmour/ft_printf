/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmapi.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yzemmour <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/31 18:55:10 by yzemmour          #+#    #+#             */
/*   Updated: 2019/04/06 22:39:58 by yzemmour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strmapi(char const *s, char (*f)(unsigned int, char))
{
	char	*p;
	int		i;

	i = 0;
	if (s)
	{
		p = (char *)malloc(ft_strlen(s) + 1);
		if (p != NULL)
		{
			while (s[i])
			{
				p[i] = f(i, s[i]);
				i++;
			}
			p[i] = '\0';
			return (p);
		}
	}
	return (NULL);
}
