/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   manup_proto.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yzemmour <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/07 22:43:46 by yzemmour          #+#    #+#             */
/*   Updated: 2019/11/07 22:53:24 by yzemmour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "manup_proto.h"
#include "check_proto.h"

void	ft_putnchr(int n, char c)
{
	while (n > 0)
	{
		ft_putchar(c);
		n--;
	}
}
int len_hex(int n) 
{    
    char hexaDeciNum[100]; 
    int i;

    i = 0;
    while(n!=0) 
    {       
        n = n/16;
		i++; 
    }
	return (i);
} 
void	manup_intiger(t_printf *s, long int v)
{
	int len;

	if (s->type == 'i' || s->type == 'd')
	{

	}
	/* manup char / manup char / manup char / manup char / manup char / manup char / manup char */
	if (s->type == 'c')
	{
		if (s->w)
		{
			if (s->m)
				ft_putchar(v);
			ft_putnchr(s->w - 1, (s->z && !s->m) ? '0' : ' ');
			if (!s->m)
				ft_putchar(v);
		}
		if (!s->w)
			ft_putchar(v);
	}
	/* manup pointeur / manup pointeur / manup pointeur / manup pointeur / manup pointeur / manup pointeur / manup pointeur */
	if (s->type == 'p')
	{
		if(s->w)
		{
			if(s->m)
			{
				ft_putstr("0x7fff");
				ft_dec_to_hexa(v);
				ft_putnchr(s->w - 14, ' ');
			}
			if(!s->m)
			{
				if (s->z)
					ft_putstr("0x");
				ft_putnchr(s->w - 14, (s->z) ? '0' : ' ');
				if (s->z)
					ft_putstr("7fff");
				if (!s->z)
					ft_putstr("0x7fff");
				ft_dec_to_hexa(v);
			}
		}
		if(!s->w)
		{
			ft_putstr("0x7fff");
			ft_dec_to_hexa(v);
		}
	}
	// type x / type x / type x / type x / type x / type x / type x / type x / type x / type x

	len = len_hex(v);
	if (s->type == 'x') // value 0 && #
	{
		if(s->w)
		{
			if(s->m)
			{
				if (s->sh && v != 0)
					ft_putstr("0x");
				if (s->pr >= s->w)
					ft_putnchr(s->pr - len_hex(v), '0');
				if (s->pr < s->w)
					ft_putnchr(s->pr - len_hex(v), '0');
				ft_dec_to_hexa(v);
				if (s->pr < s->w && len <= s->pr && s->pr != 0)
					ft_putnchr(s->w - s->pr - (!s->sh ? 0 : 2), ' ');
				if (s->pr < s->w && len > s->pr && s->pr != 0)
					ft_putnchr(s->w - len - (!s->sh ? 0 : 2), ' ');
				if (!s->pr && v != 0)
					ft_putnchr(s->w - len_hex(v) - (!s->sh ? 0 : 2), ' ');
				if (!s->pr && v == 0)
				{	
					ft_putchar('0');
					ft_putnchr(s->w - len_hex(v) - 3, ' ');
				}
				if (v == 0 && s->sh)
					ft_putnchr(2, ' ');
			}
			if(!s->m)
			{
				if (s->z && s->sh && !s->pr && v != 0)
					ft_putstr("0x");
				if (s->pr >= s->w)
				{	
					if (s->sh && v != 0)
						ft_putstr("0x");
					ft_putnchr(s->pr - len_hex(v), '0');
				}
				if (!s->pr)
				{
					if (!s->sh)
					{
						if (v == 0 && !s->z)
						{
							ft_putnchr(s->w - len_hex(v) - 1, ' ');
							ft_putchar('0');
						}
						else
							ft_putnchr(s->w - len_hex(v), (s->z) ? '0' : ' ');
					}
					if (s->sh)
						ft_putnchr(s->w - len_hex(v) - 2, (s->z) ? '0' : ' ');
					if (v == 0 && s->z && s->sh)
						ft_putstr("00");
					if (s->sh && !s->z)
						v != 0 ? ft_putstr("0x") : ft_putstr(" 0");
				}
				if (s->pr < s->w && s->pr != 0)                                                                                                                                                                                                                                                                                                                  
				{
					if (!s->sh)
					{
						ft_putnchr(s->w - (len <= s->pr ? s->pr : len), ' ');
						ft_putnchr(s->pr - len_hex(v), '0');
					}
					if (s->sh)
					{
						ft_putnchr(s->w - (len <= s->pr ? s->pr : len) - 2, ' ');
						if (v != 0)
							ft_putstr("0x");
						if (v == 0)
							ft_putstr("  ");
						ft_putnchr(s->pr - len_hex(v), '0');
					}
				}
				ft_dec_to_hexa(v);
			}
		}
		if(!s->w)
		{
			if (s->sh && v != 0)
				ft_putstr("0x");
			if (!s->pr && v == 0)
				ft_putchar('0');
			if (s->pr)
				ft_putnchr(s->pr - len_hex(v), '0');
			ft_dec_to_hexa(v);
		}
	}
 }

void	 manup_string(t_printf *s, char *str) // presicion negative problem && .0
{
	if (s->type == 's')
	{
		if (s->pr && !s->w)
			ft_putstr(ft_strsub(str,0,s->pr)); //allocation 
		if (!s->pr && s->w)
		{
				if (s->m)
					ft_putstr(str);
				ft_putnchr(s->w - ft_strlen(str), (s->z && !s->m) ? '0' : ' ');
				if (!s->m)
					ft_putstr(str);
		}
		if (s->pr && s->w)
		{
			if (s->m)
				ft_putstr(ft_strsub(str,0,s->pr)); //allocation
			ft_putnchr(s->w - ft_strlen(ft_strsub(str,0,s->pr)), (s->z && !s->m) ? '0' : ' '); //allocation
			if (!s->m)
				ft_putstr(ft_strsub(str,0,s->pr)); //allocation
		}
		if (!s->pr && !s->w)
			ft_putstr(str);
	}

}

// void	 manup_char(t_printf *s, int c)
// {
// 	if (s->type == 'c')
// 	{

// 	}
// }