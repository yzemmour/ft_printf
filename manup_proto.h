#ifndef MANUP_PROTO_H
#define MANUP_PROTO_H

#include "ft_printf.h"
#define ABS(X) ((X < 0) ? -X : X)
void  manup_intiger(t_printf *s, long int value);
void  manup_string(t_printf *s, char *str);

#endif
